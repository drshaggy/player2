[![pipeline status](https://gitlab.com/drshaggy/player2/badges/master/pipeline.svg)](https://gitlab.com/drshaggy/player2/-/pipelines)
[![coverage report](https://gitlab.com/drshaggy/player2/badges/master/coverage.svg)](https://gitlab.com/drshaggy/player2/-/commits/master)

# player2

An AI written in flutter that can play 2 player zero sum games. Its uses a Monte Carlo Tree Search algorithm to find the next move.

### Suported games
- [x] Tic Tac Toe
- [ ] Connect 4
- [ ] Chess
- [ ] Go

## Getting Started

You can play tic tac toe here https://drshaggy.gitlab.io/player2/#/tic-tac-toe-view
